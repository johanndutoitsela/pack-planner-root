package com.sela.packplannerroot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PackPlannerRootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PackPlannerRootApplication.class, args);
	}
}
